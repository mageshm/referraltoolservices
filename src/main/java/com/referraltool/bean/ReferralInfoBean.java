package com.referraltool.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;


@Entity
@Table(name = "referralinfo")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralInfoBean {

	@Id
	@Column(name="refId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonProperty("refId")
	int refId;
	
	@Column(name="repType")
	@JsonProperty("repType")
	String repType;
	
	@Column(name="dsiApp")
	@JsonProperty("dsiApp")
	String dsiApp;
	
	@JsonProperty("consumableProj")
	@Column(name="consumableProj")
	String consumableProj;
	
	@JsonProperty("referralType")
	@Column(name="referralType")
	String referralType;
	
	@JsonProperty("rsdInstruments")
	@Column(name="rsdInstruments")
	String rsdInstruments;
	
	@JsonProperty("hmdInstruments")
	@Column(name="hmdInstruments")
	String hmdInstruments;
	
	@JsonProperty("target")
	@Column(name="target")
	int target;
	
	@JsonProperty("sample")
	@Column(name="sample")
	int sample;
	
	@JsonProperty("institutionName")
	@Column(name="institutionName")
	String institutionName;
	
	@JsonProperty("accNo")
	@Column(name="accNo")
	String accNo;
	
	@JsonProperty("referredName")
	@Column(name="referredName")
	String referredName;
	
	@JsonProperty("comments")
	@Column(name="comments")
	String comments;
	
	@JsonProperty("referredemail")
	@Column(name="referredemail")
	String referredemail;	

	@JsonProperty("firstName")
	@Column(name="firstName")
	String firstName;
	
	@JsonProperty("lastName")
	@Column(name="lastName")
	String lastName;
	
	@JsonProperty("email")
	@Column(name="email")
	String email;

	@JsonProperty("nlprm")
	@Column(name="nlprm")
	String nlprm;
	
	@JsonProperty("city")
	@Column(name="city")
	String city;

	@JsonProperty("country")
	@Column(name="country")
	String country;
	
	@JsonProperty("repName")
	@Column(name="repName")
	String repName;

	@JsonProperty("repEmail")
	@Column(name="repEmail")
	String repEmail;
	
	@JsonProperty("ccrepEmail")
	@Column(name="ccrepEmail")
	String ccrepEmail;
	
	@JsonProperty("ccrepName")
	@Column(name="ccrepName")
	String ccrepName;
	
	@JsonProperty("lead_source_id")
	@Column(name="lead_source_id")
	String lead_source_id;
	
	@JsonProperty("epoch_created_date")
	@Column(name="epoch_created_date")
	long epoch_created_date;
	
	
	public String getRepName() {
		return repName;
	}
	public void setRepName(String repName) {
		this.repName = repName;
	}
	public String getRepEmail() {
		return repEmail;
	}
	public void setRepEmail(String repEmail) {
		this.repEmail = repEmail;
	}
	public String getCcrepEmail() {
		return ccrepEmail;
	}
	public void setCcrepEmail(String ccrepEmail) {
		this.ccrepEmail = ccrepEmail;
	}
	public String getCcrepName() {
		return ccrepName;
	}
	public void setCcrepName(String ccrepName) {
		this.ccrepName = ccrepName;
	}
	
	public long  getEpoch_created_date() {
		return epoch_created_date;
	}
	public void setEpoch_created_date(long epoch_created_date) {
		this.epoch_created_date = epoch_created_date;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLead_source_id() {
		return lead_source_id;
	}
	public void setLead_source_id(String lead_source_id) {
		this.lead_source_id = lead_source_id;
	}
	
	public String getNlprm() {
		return nlprm;
	}
	public void setNlprm(String nlprm) {
		this.nlprm = nlprm;
	}
	
	public String getfirstName() {
		return firstName;
	}
	public void setfirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getlastName() {
		return lastName;
	}
	public void setlastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getemail() {
		return email;
	}
	public void setemail(String email) {
		this.email = email;
	}
	
	public int getnRefId() {
		return refId;
	}
	public void setnRefId(int nRefId) {
		this.refId = nRefId;
	}
	public String getRepType() {
		return repType;
	}
	public void setRepType(String repType) {
		this.repType = repType;
	}
	public String getDsiApp() {
		return dsiApp;
	}
	public void setDsiApp(String dsiApp) {
		this.dsiApp = dsiApp;
	}
	public String getConsumableProj() {
		return consumableProj;
	}
	public void setConsumableProj(String consumableProj) {
		this.consumableProj = consumableProj;
	}
	public String getReferralType() {
		return referralType;
	}
	public void setReferralType(String referralType) {
		this.referralType = referralType;
	}
	public String getRsdInstruments() {
		return rsdInstruments;
	}
	public void setRsdInstruments(String rsdInstruments) {
		this.rsdInstruments = rsdInstruments;
	}
	public String getHmdInstruments() {
		return hmdInstruments;
	}
	public void setHmdInstruments(String hmdInstruments) {
		this.hmdInstruments = hmdInstruments;
	}
	/*public Boolean getNlprm() {
		return nlprm;
	}
	public void setNlprm(Boolean nlprm) {
		this.nlprm = nlprm;
	}*/
	public int getTarget() {
		return target;
	}
	public void setTarget(int target) {
		this.target = target;
	}
	public int getSample() {
		return sample;
	}
	public void setSample(int sample) {
		this.sample = sample;
	}
	
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getReferredName() {
		return referredName;
	}
	public void setReferredName(String referredName) {
		this.referredName = referredName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getReferredemail() {
		return referredemail;
	}
	public void setReferredemail(String referredemail) {
		this.referredemail = referredemail;
	}
	
	@Override
	 public String toString() {
		return "Refrralinfo [dsiApp=" + dsiApp + ", consumableProj="+ consumableProj + ", referralType=" + referralType+ ", rsdInstruments=" + rsdInstruments + ", hmdInstruments="+ hmdInstruments + ", target=" + target + ",sample=" + sample + ",institutionName=" + institutionName + ",accNo=" + accNo + ",referredName=" + referredName + ",comments=" + comments + ",firstName=" + firstName + ",lastName=" + lastName + ",email=" + email + ",nlprm=" + nlprm + ",referredemail="+referredemail+"]";
	 }
}
