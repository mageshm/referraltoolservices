package com.referraltool.controller;

import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.referraltool.bean.ReferralInfoBean;
import com.referraltool.model.SalesRepModel;
import com.referraltool.model.SendMailModel;
import com.referraltool.utils.DataBaseManager;
import com.referraltool.utils.Utils;

@Path("/sales")
public class SalesRepController {	
	@GET
    @Path("/getallinstitution")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllInstitution(){
		String strReturn = "";
		Connection con = null;
		Statement statement = null;;
		JSONArray joInstitutionDetails = null;
		try{
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			joInstitutionDetails = SalesRepModel.getAllInstitutionOn( statement );
			strReturn = "{\"success\":true, \"result\" : "+joInstitutionDetails +"}";
		}catch( Exception ex){
			ex.printStackTrace();
			strReturn = "{\"success\":false, \"result\" : "+ex.getMessage().replace("\"", "") +"}";
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReturn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/sendmailtoreferral")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveSendRefrerralDetails( String strRefInfo, @Context ServletContext servletContext){
		Connection con = null;
		Statement statement = null;
		String strReturn = "";
		ObjectMapper mapper = new ObjectMapper();
		JSONArray joInstitutionDetails  = null;
		try{
			
			ReferralInfoBean refBean = mapper.readValue(strRefInfo,ReferralInfoBean.class);
			refBean.setEpoch_created_date(Utils.formatDateToEpouchTime((new Date()).toString()));
				
			/*
			Date date = new Date(refBean.getEpoch_created_date());
	        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
	        String formatted = format.format(date);
	        System.out.println(formatted);
		        */
			
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			if( refBean.getReferralType().equalsIgnoreCase("CONSUMABLES") ){
				joInstitutionDetails = SalesRepModel.getSalesRepsOnProjectValue( con, statement,refBean.getInstitutionName(), refBean.getSample(),refBean.getCountry() );
				SendMailModel.sendMail( refBean, servletContext, joInstitutionDetails);
			}
			SalesRepModel.saveRefarralInfo( refBean  );
			strReturn = "{\"success\":true,\"result\":\"Mail sent successfully!!!\"}";
			
			
		}catch( Exception ex){
			ex.printStackTrace();
			strReturn = "{\"success\":false,\"result\":\""+ ex.getMessage().replace("\"", "") +"\"}";
		}
	
		return	Response.status(200).entity( strReturn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("{repType}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response exportBasedOnType( @PathParam("repType") String repType){
		Connection con = null;
		Statement statement = null;
		String strReturn = "";
		boolean bIsFileCreated  = false;
		try{
			
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			bIsFileCreated = SalesRepModel.exportBasedOnRepType(statement, repType);
			if(bIsFileCreated){
				strReturn = "{\"success\":true, \"result\" : \"File created successfully\"}";
			}else{
				strReturn = "{\"success\":false, \"result\" : \"Invalid Parameter\"}";
			}
			
			
		}catch( Exception ex){
			ex.printStackTrace();
			strReturn = "{\"success\":false,\"result\":\""+ ex.getMessage().replace("\"", "") +"\"}";
		}
	
		return	Response.status(200).entity( strReturn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
}
