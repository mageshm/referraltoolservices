package com.referraltool.controller;

import java.sql.Connection;
import java.sql.Statement;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.referraltool.model.AdminModel;
import com.referraltool.utils.DataBaseManager;
import com.referraltool.utils.QueryFilter;
import com.referraltool.utils.Utils;

@Path("/admin")
public class AdminController {
	@POST
    @Path("/getAllInstitution")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRepList( String strRepObj ){
		Connection con = null;
		Statement statement = null;
		JSONObject joReturn = null;
		JSONArray joInstitutionDetails = null;
		try{
			//System.out.println(strRepObj);
			JSONObject joFilterQuery = new JSONObject(strRepObj);
			int nLimit = Integer.parseInt(joFilterQuery.get("Limit").toString());
			int nOffset = Integer.parseInt(joFilterQuery.get("Offset").toString());
			String strSortByType = joFilterQuery.get("SortingBy").toString();
			String strSortValue = "";
			if(strSortByType.length() == 0){
				strSortByType = "institutionName";
				strSortValue = "ASC";
			}else{
				if( (joFilterQuery.get("SortingValue").toString()).equalsIgnoreCase("Ascending")){
					strSortValue = "ASC";
				}else{
					strSortValue = "DESC";
				}
			}
			
			String strFilterData = joFilterQuery.get("FilterConditions").toString();
			JSONArray jaFilterCondi = null;
			String strFilterQuery = "";
			if( strFilterData.length() > 0 ){
				jaFilterCondi = new JSONArray (joFilterQuery.get("FilterConditions").toString());
				
				//to form filter query
				 strFilterQuery = QueryFilter.getQueryFilter(jaFilterCondi);
			}
			
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			int nTotalRecords = AdminModel.getTotalAllInstitution(statement, strFilterQuery, strSortByType, strSortValue);
			joInstitutionDetails = AdminModel.getAllInstitution(statement, strFilterQuery, strSortByType, strSortValue, nLimit, nOffset);
			JSONObject joInsDetails = new JSONObject();
			joInsDetails.put("Results", joInstitutionDetails);
			joInsDetails.put("Count", nTotalRecords);
			joReturn = joInsDetails;//UtilsFactory.getJSONSuccessReturn("Institution", joInsDetails);
		}catch( Exception ex){
			ex.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( joReturn.toString() ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
}
