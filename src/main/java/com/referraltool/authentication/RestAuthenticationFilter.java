package com.referraltool.authentication;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RestAuthenticationFilter implements javax.servlet.Filter{
	//public static final String AUTHENTICATION_HEADER = "Authorization";
	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationFilter.class);
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		System.out.println("Inside");
		
		
		/*HttpServletResponse response = (HttpServletResponse) servletResponse;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS, PUT , DELETE");
		response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers ,enctype");*/
		
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		String path = request.getRequestURI();
		logger.info("Requesting " + path);
		logger.info("Request from " + request.getRemoteAddr());
		logger.info("Http method : " + request.getMethod());

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;

		/*if ( httpServletRequest.getMethod().equals("OPTIONS"))
		{
			PrintWriter outPrintWriter = response.getWriter();
			outPrintWriter.flush();
		}
		else {
			filterChain.doFilter(request, response);
		}*/
		
		filterChain.doFilter(request, servletResponse);
		
        /*if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                        System.out.println("Header: " + httprequest.getHeader(headerNames.nextElement()));
                }
        }
		filter.doFilter(request, response);*/
		
		/*if (!"admin".equals(requestheader)){
			filter.doFilter(request, response);
		}*/
		
		
		/*if (request instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String authCredentials = httpServletRequest
					.getHeader(AUTHENTICATION_HEADER);

			// better injected
			AuthenticationService authenticationService = new AuthenticationService();

			boolean authenticationStatus = authenticationService
					.authenticate(authCredentials);

			if (authenticationStatus) {
				filter.doFilter(request, response);
			} else {
				if (response instanceof HttpServletResponse) {
					HttpServletResponse httpServletResponse = (HttpServletResponse) response;
					httpServletResponse
							.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}
		}*/
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
