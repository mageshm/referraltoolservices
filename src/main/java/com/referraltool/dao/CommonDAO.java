package com.referraltool.dao;

import java.sql.ResultSet;
import java.sql.Statement;

public class CommonDAO {
	public static ResultSet toGetobjects(Statement statement, String strQuery){
		ResultSet rs = null;
		try{				
			 rs = statement.executeQuery(strQuery);
		}catch( Exception ex ){
			ex.printStackTrace();
		}		
		return rs;
		
	}
}
