package com.referraltool.dao;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

public class UtilsDAO {
HibernateTemplate template;
	
	public HibernateTemplate getTemplate() {
		return template;
	}
	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}
	public void saveObject(Object obj){
		template.save(obj);
	}
	
	public void updateJob(Object obj){
		template.update(obj);
	}
	public void saveUpdateObject(Object obj){
		template.saveOrUpdate(obj);
	}
}
