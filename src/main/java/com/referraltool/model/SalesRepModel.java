package com.referraltool.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.referraltool.bean.ReferralInfoBean;
import com.referraltool.dao.CommonDAO;
import com.referraltool.dao.UtilsDAO;
import com.referraltool.utils.Constant;
import com.referraltool.utils.DataBaseManager;
import com.referraltool.utils.Utils;

public class SalesRepModel {

	public static JSONArray getSalesRepsOnProjectValue(Connection con, Statement statement,	String strInstitutionName, 
			int nSampleTargets, String strRegion) throws Exception {
		JSONArray jaRepDetails = null;
		JSONObject joRepDetail = null;
		try{
			jaRepDetails = new JSONArray();
		
			
			CallableStatement cStmt = con.prepareCall("{call toGetRepNameForReferal(?,?,?,?,?,?,?,?)}");
			cStmt.setString(1, strInstitutionName);
			cStmt.setInt(2, nSampleTargets);
			cStmt.setString(3, strRegion);
			cStmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			cStmt.registerOutParameter(5, java.sql.Types.VARCHAR);
			cStmt.registerOutParameter(6, java.sql.Types.VARCHAR);
			cStmt.registerOutParameter(7, java.sql.Types.VARCHAR);
			cStmt.registerOutParameter(8, java.sql.Types.VARCHAR);
						
			cStmt.executeUpdate();
			
			joRepDetail = new JSONObject();
			
			joRepDetail.put("AM_Member_Name", cStmt.getString(4));
			joRepDetail.put("AM_Member_eMail", cStmt.getString(5));
			joRepDetail.put("SGN_Name", cStmt.getString(6));
			joRepDetail.put("CCREP_Name", cStmt.getString(7));
			joRepDetail.put("CCREP_EMAIL", cStmt.getString(8));
			
			jaRepDetails.put(joRepDetail);
			
			
		}catch (Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return jaRepDetails;
	}

	public static boolean saveRefarralInfo(ReferralInfoBean refBean) {
		boolean bIsMailSent = false;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.saveObject(refBean);
			bIsMailSent  = true;
		}catch ( Exception ex){
			ex.printStackTrace();
		}
		return bIsMailSent;
	}

	public static JSONArray getAllInstitutionOn(Statement statement) throws Exception {
		JSONArray jaInsRecords = null;
		StringBuffer sbQuery = null;
		JSONObject joInstRecord = null;
		ResultSet rs = null;
		try{
			jaInsRecords = new JSONArray();
			sbQuery = new StringBuffer();
			sbQuery.append(" SELECT * FROM instrument ");
			rs = CommonDAO.toGetobjects(statement,sbQuery.toString());
			int i =0;
			while( rs.next() ){
				joInstRecord = new JSONObject();
			    joInstRecord.put("type", rs.getString("type"));
			    joInstRecord.put("category", rs.getString("category"));
			    joInstRecord.put("description", rs.getString("description"));
			    joInstRecord.put("lead_source_id", rs.getString("lead_source_id"));
			    joInstRecord.put("lead_source_name", rs.getString("lead_source_name"));
			    joInstRecord.put("part_number", rs.getString("part_number"));
				
				jaInsRecords.put(joInstRecord);
			}
		}catch (Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);
		}
		return jaInsRecords;
	}

	public static boolean exportBasedOnRepType(Statement statement,String repType) throws Exception {
		boolean bIsFileCreated = false;
		StringBuffer sbQuery = null;
		ResultSet rs = null;
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy") ;
		String curDate =dateFormat.format(date);
		boolean flag = false;
		String[] headerData = {"lead_source_id","first_name","last_name","email","institution","city","country","purchase_intent","comments","fisher_account","opportunity_notes","lead_creation_date"};
		try{
			File statText = new File(Constant.FILE_PATH +"Lead_referrals_"+repType+"_"+curDate+".csv");
			if( !statText.exists()) {
				statText.createNewFile();
				flag=true;
			}
			FileOutputStream is = new FileOutputStream(statText,true);
			OutputStreamWriter osw = new OutputStreamWriter(is);    
			Writer w = new BufferedWriter(osw);
			
			if(flag==true){
				for (String header : headerData) {
					w.write(header);
					w.write("|");
				}
				w.write("\n");
			}
			
			sbQuery = new StringBuffer();
			if(repType.equalsIgnoreCase("HMD")){
				sbQuery.append(" SELECT lead_source_id,firstName,lastName,email,institutionName,city,country,repType,comments,accNo,hmdInstruments,referredName,sample, target,dsiApp,nlprm, epoch_created_date FROM referralinfo where referralType = 'Instruments' AND repType = '")
				.append(repType).append("' ");
			}else{
				sbQuery.append(" SELECT lead_source_id,firstName,lastName,email,institutionName,city,country,repType,comments,accNo,rsdInstruments,referredName, sample, target,dsiApp,nlprm, epoch_created_date FROM referralinfo where referralType = 'Instruments' AND repType = '")
				.append(repType).append("' ");
			}
			rs = CommonDAO.toGetobjects(statement,sbQuery.toString());
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while( rs.next() ){
				for(int i=1;i<=columnsNumber;i++){
					if( i == 11 ){
						if(repType.equalsIgnoreCase("RSD")){
							String instrumentName = Utils.replaceNullValue(rs.getString(i), "");
                            String opportunityNotes = "Channel Sales partner lead for GSD/CSD - product selected:" + instrumentName 
                            		+ " - referred_rep:" + Utils.replaceNullValue(rs.getString(12), "") 
                            		+ " - no_targets:" + Utils.replaceNullValue(rs.getString(14), "")
                            		+ " - no_samples:" + Utils.replaceNullValue(rs.getString(13), "")
                            		+ " - comments:" + Utils.replaceNullValue(rs.getString(9), "");
                            w.write(opportunityNotes);
    						w.write('|');
						}else if(repType.equalsIgnoreCase("HMD")){
							String instrumentName = rs.getString(i);
                            String opportunityNotes = "Channel Sales partner lead for GSD/CSD - product selected:" + instrumentName 
                            		+ " - selected dsi app:" + Utils.replaceNullValue(rs.getString(15), "") 
                            		+ " - new lab:" + Utils.replaceNullValue(rs.getString(16), "")
                            		+ " - no_targets:" + Utils.replaceNullValue(rs.getString(14), "")
                            		+ " - no_samples:" + Utils.replaceNullValue(rs.getString(13), "")
                            		+ " - referred_rep:" + Utils.replaceNullValue(rs.getString(12), "")
                            		+ " - comments:" + Utils.replaceNullValue(rs.getString(9), "");
                            w.write(opportunityNotes);
    						w.write('|');
						}
						
					}else if(i == 12 || i == 13 || i == 14|| i == 15|| i == 16){
					
					}else{
						w.write(Utils.replaceNullValue(rs.getString(i), "null"));
						w.write('|');
					}
				 }
				w.write('\n');
			}
			w.write("\n");
			w.close();
			bIsFileCreated = true;
		}catch (Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);
		}
		return bIsFileCreated;
	}
}
