package com.referraltool.model;

import java.sql.ResultSet;
import java.sql.Statement;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.referraltool.dao.CommonDAO;
import com.referraltool.utils.DataBaseManager;
import com.referraltool.utils.Utils;



public class AdminModel {

	public static JSONArray getAllInstitution(Statement statement,	String strFilterQuery, String strSortByType, String strSortValue, int nLimit, int nOffset) throws Exception {
		JSONArray jaInsDetails = null;
		StringBuffer sbQuery = null;
		JSONObject joInDetail = null;
		ResultSet rs = null;
		try{
			jaInsDetails = new JSONArray();
			sbQuery = new StringBuffer();
			sbQuery.append(" SELECT * FROM referralinfo WHERE 1=1 ")
				.append( strFilterQuery )
				.append(" ORDER BY ").append(strSortByType).append(" ").append(strSortValue);
			if( nLimit > 0 ){
				sbQuery.append(" LIMIT ").append(nLimit);
				sbQuery.append(" OFFSET ").append(nOffset);
			}
			//System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects(statement,sbQuery.toString());
			jaInsDetails = Utils.convertResultSetIntoJSON(rs);
			
			/*
			int i =0;
			while( rs.next() ){
				joInDetail = new JSONObject();
				joInDetail.put("Customer_Postal_Code", rs.getString("Customer_Postal_Code"));
				joInDetail.put("Id", rs.getString("Id"));
				joInDetail.put("SGN_Name", rs.getString("SGN_Name"));
				joInDetail.put("Ship_To_Customer_Name", rs.getString("Ship_To_Customer_Name"));
				joInDetail.put("Customer_Country_Name", rs.getString("Customer_Country_Name"));
				joInDetail.put("Customer_State_Province_Code", rs.getString("Customer_State_Province_Code"));
				joInDetail.put("AM_Territory_Type_Code", rs.getString("AM_Territory_Type_Code"));
				joInDetail.put("AM_Territory_Name", rs.getString("AM_Territory_Name"));
				joInDetail.put("AM_Member_Name", rs.getString("AM_Member_Name"));
				joInDetail.put("AM_Member_eMail", rs.getString("AM_Member_eMail"));
				
				joInDetail.put("reffredemail", rs.getString("reffredemail"));
				joInDetail.put("repname", rs.getString("repname"));
				joInDetail.put("repemail", rs.getString("repemail"));
				joInDetail.put("cusname", rs.getString("cusname"));
				joInDetail.put("cusemail", rs.getString("cusemail"));
				joInDetail.put("cusinstitution", rs.getString("cusinstitution"));
				joInDetail.put("mailsentdate", rs.getString("mailsentdate"));
				
				
				jaInsDetails.put(joInDetail);
			}*/
		}catch (Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);
		}
		return jaInsDetails;
	}

	public static int getTotalAllInstitution(Statement statement,	String strFilterQuery, String strSortByType, String strSortValue) throws Exception {
		
		StringBuffer sbQuery = null;
		ResultSet rs = null;
		int nTotalRecord = 0;
		try{
			sbQuery = new StringBuffer();
			//sbQuery.append(" SELECT count(*) as nTotalRecords FROM sales_rep_byinstitution WHERE 1=1 " )
					//.append(strFilterQuery);
			sbQuery.append(" SELECT count(*) as nTotalRecords FROM referralinfo WHERE 1=1 " )
			.append(strFilterQuery)
			.append(" ORDER BY ").append(strSortByType).append(" ").append(strSortValue);
			//System.out.println("TotalRecords : " + sbQuery.toString());
			rs = CommonDAO.toGetobjects(statement,sbQuery.toString());
			while( rs.next() ){
				nTotalRecord = rs.getInt("nTotalRecords");
			}
		}catch (Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);
		}
		return nTotalRecord;
	}
}
