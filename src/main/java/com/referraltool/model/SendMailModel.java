package com.referraltool.model;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.codehaus.jettison.json.JSONArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;

import com.referraltool.bean.ReferralInfoBean;
import com.referraltool.utils.Constant;

public class SendMailModel {
	public static boolean sendMail(ReferralInfoBean refBean, ServletContext context, JSONArray joInstitutionDetails) throws Exception {
		boolean isEmailSent = false;
		try{
			//System.out.println("joInstitutionDetails : " + joInstitutionDetails);
			Properties props = System.getProperties();
	    	props.put("mail.transport.protocol", "smtp");
	    	props.put("mail.smtp.port", Constant.PORT); 
	    	props.put("mail.smtp.auth", "true");
	    	props.put("mail.smtp.starttls.enable", "true");
	    	props.put("mail.smtp.starttls.required", "true");
	
	    	Session session = Session.getDefaultInstance(props);
	    	
	    	//String strPath = context.getRealPath("/WebContent/EmailTemp/mailtemplate.html");
	    	//System.out.println(context.getRealPath("/EmailTemp/mailtemplate.html"));
	    	String strPath = "";
	    	BufferedReader bufferedReader = null;
	    	StringBuffer strBody = new StringBuffer("");
	    	strPath = context.getRealPath("/EmailTemp/mailtemplate.html");
	    	bufferedReader = new BufferedReader(new FileReader(strPath));
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				line = line.replaceAll("@TONAME@",(joInstitutionDetails.getJSONObject(0).get("AM_Member_Name")).toString());
				line = line.replaceAll("@REFERERNAME@",refBean.getReferredName());
				line = line.replaceAll("@INSTITUTIONNAME@",refBean.getInstitutionName());
				line = line.replaceAll("@CUSTOMERNAME@",refBean.getfirstName()+" "+refBean.getlastName());
				line = line.replaceAll("@CUSTOMEREMAIL@",refBean.getemail());
				line = line.replaceAll("@TYPE@",refBean.getConsumableProj());
				line = line.replaceAll("@TARGETS@",refBean.getTarget()+"");
				line = line.replaceAll("@SAMPLES@",refBean.getSample()+"");
				line = line.replaceAll("@LINK@","");
				line = line.replaceAll("@REFERERCOMMENTS@",refBean.getComments());
				strBody.append(line);
			}
	    	
			bufferedReader.close();
			
	    	//String[] addressCCArray = ("palani@ceino.com").split(",");
	    	String[] addressCCArray = (joInstitutionDetails.getJSONObject(0).get("CCREP_EMAIL").toString()).split(",");
	    	int nTotalCCRecipient = addressCCArray.length;
	    	
	    	InternetAddress[] addressCC = new InternetAddress[ (nTotalCCRecipient+1) ];
	    	for( int c = 0; c < addressCCArray.length; c++){
	    		addressCC[c] = new InternetAddress(addressCCArray[c]);
	    	}
	    	addressCC[nTotalCCRecipient] = new InternetAddress(refBean.getReferredemail());
	    	
	    	System.out.println( "ToMail : " +  (joInstitutionDetails.getJSONObject(0).get("AM_Member_eMail").toString() ));
	    	System.out.println( "CcEmail : " +  (joInstitutionDetails.getJSONObject(0).get("CCREP_EMAIL").toString()) );
			//InternetAddress addressTo = new InternetAddress("sankar@ceino.com");//
			InternetAddress addressTo = new InternetAddress(joInstitutionDetails.getJSONObject(0).get("AM_Member_eMail").toString());
			
	    	MimeMessage msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(Constant.FROM,Constant.FROM_NAME));
	        msg.setRecipient(Message.RecipientType.TO, addressTo);
	        msg.setRecipients(Message.RecipientType.CC, addressCC);
	        msg.setSubject(Constant.SUBJECT);
	        msg.setContent(strBody.toString(),"text/html; charset=utf-8");
	        
	        Transport transport = session.getTransport();
	        transport.connect(Constant.HOST, Constant.SMTP_USERNAME, Constant.SMTP_PASSWORD);
	        transport.sendMessage(msg, msg.getAllRecipients());
	        isEmailSent = true;
	        System.out.println("Email Sent");
		}catch( Exception ex){
			 isEmailSent = false;
			 ex.printStackTrace();
			 throw ex;
		}
		
		return isEmailSent;
	}
}
