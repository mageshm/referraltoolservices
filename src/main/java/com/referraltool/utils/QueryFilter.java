package com.referraltool.utils;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

public class QueryFilter {
	public static String getQueryFilter( JSONArray joFilterCondit){
		//System.out.println(joFilterCondit);
		StringBuffer sbFilterQuery = new StringBuffer();
		try{
			for(int i=0; i<joFilterCondit.length();i++){
				sbFilterQuery.append(" AND ");
				
				JSONObject joFilterQuery = new JSONObject(joFilterCondit.get(i).toString());
				
				if( (joFilterQuery.get("FilterType").toString()).length() != 0 ){
					if( (joFilterQuery.get("FilterType").toString()).equalsIgnoreCase("epoch_created_date") ){
						String[] strFilterValue = (joFilterQuery.get("FilterValue").toString()).split("AND");
						sbFilterQuery.append("( DATE_FORMAT(FROM_UNIXTIME(floor(epoch_created_date/1000)),'%Y-%m-%d') BETWEEN DATE_FORMAT('").append(strFilterValue[0].trim()).append("','%Y-%m-%d') AND DATE_FORMAT('").append(strFilterValue[1].trim()).append("','%Y-%m-%d')) ") ;
					
					}else{
						String[] strFilterValue = (joFilterQuery.get("FilterValue").toString()).split(",");
						if( strFilterValue.length > 1 ){
							sbFilterQuery.append(joFilterQuery.get("FilterType").toString())
								.append(" REGEXP '");
							String strValues = "";
							//	System.out.println( Arrays.asList(strFilterValue));
							for(int si = 0; si < strFilterValue.length; si++){
								strValues = strValues + strFilterValue[si] ;
								
								if( (si+1) == strFilterValue.length ){
									
								}else{
									strValues = strValues + '|';
								}
							}
							sbFilterQuery.append(strValues).append("'");
						}else{
							sbFilterQuery.append(joFilterQuery.get("FilterType").toString())
							.append(" LIKE '%").append(joFilterQuery.get("FilterValue").toString()).append("%'");
						}
					}
				}
				
				/*if( (i+1) != joFilterCondit.size() ){
					sbFilterQuery.append(" AND ");
				}*/
			}
		}catch( Exception ex ){
			ex.printStackTrace();
		}
		//System.out.println("sbFilterQuery : " + sbFilterQuery.toString());
		return sbFilterQuery.toString();
	}
}
