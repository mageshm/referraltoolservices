package com.referraltool.utils;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

public class Utils {
	
	public static long formatDateToEpouchTime( String strDate){
		long nEpouchTime = 0;
		try{
			DateFormat dF = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy");
			Date date = dF.parse(strDate);
			nEpouchTime = date.getTime();
		}catch( Exception ex){
			ex.printStackTrace();
		}
		return nEpouchTime;
	}
	
	/**
     * Convert a result set into a JSON Array
     * @param resultSet
     * @return a JSONArray
     * @throws Exception
     */
    public static JSONArray convertResultSetIntoJSON(ResultSet resultSet) throws Exception {
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()) {
            int total_rows = resultSet.getMetaData().getColumnCount();
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                String columnName = resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase();
                Object columnValue = resultSet.getObject(i + 1);
                // if value in DB is null, then we set it to default value
                if (columnValue == null){
                    columnValue = "null";
                }
                /*
                Next if block is a hack. In case when in db we have values like price and price1 there's a bug in jdbc - 
                both this names are getting stored as price in ResulSet. Therefore when we store second column value,
                we overwrite original value of price. To avoid that, i simply add 1 to be consistent with DB.
                 */
                if (obj.has(columnName)){
                    columnName += "1";
                }
                obj.put(columnName, columnValue);
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }

    public static String replaceNullValue( String data, String ReplaceData){
    	
    	String strReturn = "";
    	if(data==null || data.equalsIgnoreCase("NULL")){
    		strReturn = ReplaceData;
    	}else{
    		strReturn = data;
    	}
    	return strReturn;
    }
}
